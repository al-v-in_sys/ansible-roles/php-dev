# PHP Dev Box - Ansible role

Role to setup the basics of a local dev environment for a PHP website.

### MySQL helper scripts
`import-db-from-remote.sh` will allow quick import and modification of a remote database. Access details to the remote MySQL server must be placed in `~/.mysql-remote.cnf` (see `~/.mysql-remote.sample.cnf`). 

You can give the local dev server access to the MySQL server (through the firewall etc) or you can use port forwarding. Set it up on your host machine to via: `ssh -NL 0.0.0.0:33061:localhost:3306 remote-machine`, then set the MySQL server host to `192.168.33.1:33601` in `~/.mysql-remote.cnf`.

`dump-db.sh` will dump and gzip the local MySQL database to `/var/www/db/devdb.sql.gz`.


### Mail
View caught mail at port 8025 eg: [http://192.168.33.10:8025/](http://192.168.33.10:8025/), with MailHog
